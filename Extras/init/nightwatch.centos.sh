#!/bin/bash
#description: Nightwatch statistics gathering
#processname: fastcomet-nightwatch

LVDAEMON="/opt/fastcomet/nightwatch/Fastcomet/Nightwatch.pl"

. /etc/rc.d/init.d/functions

case "$1" in
        start)
                echo -n "Starting Nightwatch: "
                $LVDAEMON
                RET=$?
                [ $RET -eq 0 ] && success || failure
                echo
                exit $RET
        ;;
        debug)
                echo -n "Starting Nightwatch (With Debug Flag): "
                $LVDAEMON Debug
                RET=$?
                [ $RET -eq 0 ] && success || failure
                echo
                exit $RET
        ;;
        stop)
                echo -n "Stopping Nightwatch: "
                kill `cat /var/run/nightwatch.pid` 2>/dev/null
                RET=$?
                [ $RET -eq 0 ] && success || failure
                [ $RET -eq 0 ] && rm /var/run/nightwatch.pid
                echo
                exit $RET
        ;;
        restart)
                $0 stop
                $0 start
        ;;
        status)
                status -p /var/run/nightwatch.pid fastcomet-nightwatch
        ;;
        update)
                $0 stop
                echo "Updating Nightwatch core files (configs will be preserved)"
                cd /opt/fastcomet/nightwatch && /usr/bin/git pull
                $0 start
        ;;
        *)
                echo $"Usage: $0 {start|stop|restart|status|update}"
        ;;
esac