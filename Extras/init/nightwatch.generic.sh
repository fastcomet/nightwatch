#!/bin/bash

SUCCESS="\E[32;40mSuccess\E[0m"
FAILED="\E[31;40mFailed\E[0m"

LV="/opt/fastcomet/nightwatch/Fastcomet/Nightwatch.pl"

case "$1" in
	start)
		echo -n $"Starting nightwatch: "
		$LV
		RET=$?
		[ $RET -eq 0 ] && echo -e $SUCCESS || echo -e $FAILED
		exit $RET
	;;
	debug)
		echo -n $"Starting nightwatch (With Debug Flag): "
		$LV Debug
		RET=$?
		[ $RET -eq 0 ] && echo -e $SUCCESS || echo -e $FAILED
		exit $RET
	;;
	stop)
		echo -n $"Stopping nightwatch: "
		kill `cat /var/run/nightwatch.pid` 2>/dev/null
		RET=$?
		[ $RET -eq 0 ] && echo -e $SUCCESS || echo -e $FAILED
		exit $RET
	;;
	restart)
		$0 stop
		$0 start
	;;
	status)
		[ ! -e /var/run/nightwatch.pid ] && echo "No nightwatch pid file: status unknown" && exit 1
		PID=`cat /var/run/nightwatch.pid`
		grep "fastcomet-nightwatch" "/proc/$PID/cmdline" 2>/dev/null
		RET=$?
		[ $RET -eq 0 ] && echo "Nightwatch is running" || echo "Nightwatch is not running"
		exit $RET
	;;
	*)
		echo $"Usage: $0 {start|stop|restart|status}"
	;;
esac