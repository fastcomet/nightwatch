Name:     fastcomet-nightwatch
Version:  1.1
Release:  5
Summary:  Fastcomet Nightwatch Agent
License:  GPLv2+
BuildArch: noarch

Requires: perl(Crypt::SSLeay),perl(DBD::mysql),perl(LWP::Protocol::https)
Obsoletes: nightwatch

%description
The Fastcomet Nightwatch data collection agent

%prep
http_fetch() {
	mkdir -p `dirname $2`
	if command -v wget >/dev/null 2>&1; then
		wget -q -4 -O $2 $1 || { 
			echo >&2 "Failed to fetch $1. Aborting install.";
			exit 1;
		}
	elif command -v curl >/dev/null 2>&1; then
		curl -sf4L $1 > $2 || { 
			echo >&2 "Failed to fetch $1. Aborting install.";
			exit 1;
		}
	else
		echo "Unable to find curl or wget, can not fetch needed files"
		exit 1
	fi
}
[ -e $OLDPWD/Extras/lib/perl5/Linux/Distribution.pm  ] || http_fetch http://cpansearch.perl.org/src/CHORNY/Linux-Distribution-0.23/lib/Linux/Distribution.pm $OLDPWD/Extras/lib/perl5/Linux/Distribution.pm
[ -e $OLDPWD/Extras/lib/perl5/Try/Tiny.pm ] || http_fetch http://cpansearch.perl.org/src/ETHER/Try-Tiny-0.24/lib/Try/Tiny.pm $OLDPWD/Extras/lib/perl5/Try/Tiny.pm

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/fastcomet/nightwatch/lib/perl5/
mkdir -p %{buildroot}/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/DataGetter/Packages/
mkdir -p %{buildroot}/opt/fastcomet/nightwatch/Extras/
mkdir -p %{buildroot}/etc/init.d/
mkdir -p %{buildroot}/etc/fastcomet/nightwatch.d
cp $OLDPWD/Extras/init/nightwatch.centos.sh %{buildroot}/etc/init.d/nightwatch
cp -r $OLDPWD/Extras/lib/perl5/* %{buildroot}/opt/fastcomet/nightwatch/lib/perl5/
cp -r $OLDPWD/Fastcomet %{buildroot}/opt/fastcomet/nightwatch/
cp $OLDPWD/Extras/Modules/Packages/YUM.pm %{buildroot}/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/DataGetter/Packages/YUM.pm
cp $OLDPWD/Extras/app-report.pl %{buildroot}/opt/fastcomet/nightwatch/Extras/app-report.pl
cp $OLDPWD/Extras/conf/* %{buildroot}/etc/fastcomet/nightwatch.d/

%files
/etc/init.d/nightwatch
/opt/fastcomet/nightwatch/lib/perl5/Linux/Distribution.pm
/opt/fastcomet/nightwatch/lib/perl5/Try/Tiny.pm
/opt/fastcomet/nightwatch/Fastcomet/Nightwatch.pl
/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/*.pm
/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/DataGetter/*.pm
/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/DataGetter/Applications/*.pm
/opt/fastcomet/nightwatch/Fastcomet/Nightwatch/DataGetter/Packages/YUM.pm
%config %attr(640,root,root) /etc/fastcomet/nightwatch.d/*.conf
/opt/fastcomet/nightwatch/Extras/app-report.pl

%post
if [ $1 -eq 1 ] ; then 
    # Initial installation 
    service nightwatch start || :
    chkconfig --add nightwatch
    chkconfig --level 35 nightwatch on
fi
chmod -R o-rwx /etc/fastcomet/nightwatch.d/

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    service nightwatch stop || :
    chkconfig --del nightwatch
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    service nightwatch restart || :
fi
