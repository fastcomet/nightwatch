package Fastcomet::Nightwatch::STDERRLogger;

use strict;
use warnings;

use Fastcomet::Nightwatch::Util;

sub TIEHANDLE {
	my $class = shift;
	bless [], $class;
}

sub PRINT {
	my $self = shift;
	return unless defined $Fastcomet::Nightwatch::Util::logger;
	chomp(@_);
	$Fastcomet::Nightwatch::Util::logger->warn(@_);
}

sub OPEN {
	my $self = shift;
}

1;
