package Fastcomet::Nightwatch::DataGetter::Packages::YUM;

=head1 Perl Artistic License

Copyright 2013 Linode, LLC.  Longview is made available under the terms
of the Perl Artistic License, or GPLv2 at the recipients discretion.

=head2 Perl Artistic License

Read it at L<http://dev.perl.org/licenses/artistic.html>.

=head2 GNU General Public License (GPL) Version 2

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

See the full license at L<http://www.gnu.org/licenses/>.

=cut

use strict;
use warnings;

use Fastcomet::Nightwatch::Util ':BASIC';

our $DEPENDENCIES = [];

our $cache_touch_dt;

my $yum_pid_file    = '/var/run/yum.pid';

sub get {
	my (undef, $dataref) = @_;

	$logger->trace('Entering YUM module');

  if(check_running($dataref)){
		$logger->debug('Another instance of YUM is already running, skip package this pass');
		$dataref->{packages} = [];
		return $dataref;
	}

	return $dataref if defined $cache_touch_dt && get_DB_touch_dt() == $cache_touch_dt;

	my %u_pkgs = upgradable_pkgs();
	if (!%u_pkgs){
		$dataref->{packages} = [];
		$cache_touch_dt = get_DB_touch_dt();
		return $dataref;
	}
	my %c_pkgs = current_pkgs();
	$dataref->{packages} = [
		map {
			{
				name => $_,
				current => $c_pkgs{$_},
				new => $u_pkgs{$_}
			}
		}
		keys %u_pkgs
	];

	$cache_touch_dt = get_DB_touch_dt();
	return $dataref;
}

sub get_DB_touch_dt {
	my $oldest = 0;
	my $candidate;
	for my $db (glob('/var/lib/rpm/__db.*')) {
		$candidate = (stat($db))[9];
		$oldest = $candidate if $oldest < $candidate;
	}
	return $oldest;
}

sub current_pkgs {
	# yum wraps unless we fake a tty
	my @pkg_list = qx(script -c 'yum --color=never list installed' /dev/null);
	my %pkgs;
	for my $pkg (@pkg_list) {
		if ( $pkg =~ m/^(.*?)\s+(.*?)\s+(.*)$/ ) {
			$pkgs{$1} = $2;
		}
	}
	return %pkgs;
}

sub upgradable_pkgs {
	my @pkg_list = qx(yum check-update);
	my %pkgs;
	for my $pkg (@pkg_list) {
		if ( $pkg =~ m/^(\S+\.\S+)\s+(\S+)\s+\S+\s*$/ ) {
			$pkgs{$1} = $2;
		}
	}
	return %pkgs;
}

sub check_running {
	return 0 unless (-e $yum_pid_file);
	return slurp_file($yum_pid_file);
}

1;
