package Fastcomet::Nightwatch::DataGetter::Load;

=head1 Perl Artistic License

Copyright 2013 Linode, LLC.  Longview is made available under the terms
of the Perl Artistic License, or GPLv2 at the recipients discretion.

=head2 Perl Artistic License

Read it at L<http://dev.perl.org/licenses/artistic.html>.

=head2 GNU General Public License (GPL) Version 2

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

See the full license at L<http://www.gnu.org/licenses/>.

=cut

use strict;
use warnings;
use Fastcomet::Nightwatch::Util ':BASIC';

our $DEPENDENCIES = [];

sub get {
	my (undef, $dataref) = @_;

	$logger->trace('Collecting system load info');
    my @load = ( split( /\s/, ( slurp_file( $PROCFS . 'loadavg' ) )[0], 4 ) );
	my $loadMinute = $load[0] or do {
		$logger->info("Couldn't check ${PROCFS}loadavg: $!");
		return $dataref;
	};
    $dataref->{load} = [];
	push @{$dataref->{load}}, $loadMinute + 0;
	push @{$dataref->{load}}, $load[1] + 0;
	push @{$dataref->{load}}, $load[2] + 0;

	return $dataref;
}

1;
