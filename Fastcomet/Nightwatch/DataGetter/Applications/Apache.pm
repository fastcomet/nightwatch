package Fastcomet::Nightwatch::DataGetter::Applications::Apache;

=head1 COPYRIGHT/LICENSE

Copyright 2013 Linode, LLC.  Longview is made available under the terms
of the Perl Artistic License, or GPLv2 at the recipients discretion.

=head2 Perl Artistic License

Read it at L<http://dev.perl.org/licenses/artistic.html>.

=head2 GNU General Public License (GPL) Version 2

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

See the full license at L<http://www.gnu.org/licenses/>.

=cut

use strict;
use warnings;

use Fastcomet::Nightwatch::Util qw(:APPLICATIONS get_UA);

our $DEPENDENCIES = ['Processes.pm'];
our $SIGNATURES    = [ 'apache2', 'httpd' ];

my $default_location    = 'http://127.0.0.1/whm-server-status?auto';
my $determined_location = undef;
my @cared_about         = ( "Total Accesses", "Total kBytes", "Scoreboard" );
my $scoreboard_lookup   = {
	"_" => 'waiting_for_connection',
	"S" => 'starting_up',
	"R" => 'reading_request',
	"W" => 'sending_reply',
	"K" => 'keepalive',
	"D" => 'dns_lookup',
	"C" => 'closing_connection',
	"L" => 'logging',
	"G" => 'gracefully_finishing',
	"I" => 'idle_cleanup_of_worker',

	# don't capture placeholder slots
	# "." => Open slot with no current process,
};

sub get {
	my ( undef, $dataref ) = @_;

	my $config_file = get_config_file_name();

	$logger->trace('Collecting Apache information');

	unless ($determined_location) {
		my $site_location = get_config_data($config_file);
		unless ( exists( $site_location->{location} ) ) {
			$logger->warn(
				"Unable to find the location of the Apache statistics page, defaulting to $default_location"
			);
			$determined_location = $default_location;
		}
		else {
			$determined_location = $site_location->{location};
			if ( $determined_location !~ /\?auto$/ ) {
				$determined_location .= '?auto';
				$logger->warn(
					"The location specified in $config_file should end in '?auto'. Automatically appending '?auto' for Apache checks"
				);
			}
		}
	}
	my $ua  = get_UA();

	# temporarily skip checks for self-signed certs
	$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
	my $res = $ua->get($determined_location);
	$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 1;

	unless ( $res->is_success ) {
		return application_error(
			$dataref,
			'apache',
			"Unable to access local server status for Apache at $determined_location: "
				. $res->status_line(),
			1
		);
	}
	unless ( $res->content =~ /Scoreboard:/ ) {
		return application_error(
			$dataref,
			'apache',
			"The Apache status page doesn't look right. Check $determined_location and investigate any redirects for misconfiguration.",
			2
		);
	}
	my $server_version = $res->header('Server');
	$dataref->{applications}->{apache}->{version} = $server_version if ($server_version);
	foreach my $line ( split( /\n/, $res->content() ) ) {
		if($line =~ /^([^:]+):\s*(\S*)/) {
			my ( $key, $value ) = ( $line =~ /^([^:]+):\s*(\S*)/ );
			next unless grep {/$key/} @cared_about;
            $key =~ s/\s/_/g;
            $key =~ s/(\w)/\L$1/g;
			$dataref->{applications}->{apache}->{lcfirst($key)} = $value;
		}
	}

	for my $char (split( '', $dataref->{applications}->{apache}->{scoreboard} ) ) {
		next unless exists $scoreboard_lookup->{$char};
		$dataref->{applications}->{apache}->{workers}->{$scoreboard_lookup->{$char}} += 1;
	}
	for my $key ( keys(%$scoreboard_lookup) ) {
		$dataref->{applications}->{apache}->{workers}->{$scoreboard_lookup->{$key}} += 0;
	}
	delete $dataref->{applications}->{apache}->{scoreboard};
	$dataref->{applications}->{apache}->{status} = 0;
	$dataref->{applications}->{apache}->{message} ||= '';
	return $dataref;
}

1;
