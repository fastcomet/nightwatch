package Fastcomet::Nightwatch::DataGetter::SysInfo;

=head1 Perl Artistic License

Copyright 2013 Linode, LLC.  Longview is made available under the terms
of the Perl Artistic License, or GPLv2 at the recipients discretion.

=head2 Perl Artistic License

Read it at L<http://dev.perl.org/licenses/artistic.html>.

=head2 GNU General Public License (GPL) Version 2

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

See the full license at L<http://www.gnu.org/licenses/>.

=cut

use strict;
use warnings;

use Sys::Hostname;

use Fastcomet::Nightwatch::Util ':SYSINFO';
use Fastcomet::Linux::Distribution qw(distribution_name distribution_version);
use POSIX 'uname';

our $DEPENDENCIES = [];

sub get {
	my (undef, $dataref) = @_;
	$logger->trace('Collecting SysInfo');

	$dataref->{sysInfo}->{os}->{dist}			= 'unknown';
	$dataref->{sysInfo}->{os}->{version} 	= 'unknown';
	if ( my $distro = distribution_name() ) {
		$dataref->{sysInfo}->{os}->{dist} 		= ucfirst($distro);
		$dataref->{sysInfo}->{os}->{version} = distribution_version();
	}

	$dataref->{sysInfo}->{client} = $VERSION;

	my ( $sysname, undef, $release, $version, $machine ) = uname();
	$dataref->{sysInfo}->{kernel}   = "$sysname $release";
	$dataref->{sysInfo}->{type}     = detect_system();
	$dataref->{sysInfo}->{arch}     = $ARCH;
	$dataref->{sysInfo}->{hostname} = hostname;

	my @cpu_info = slurp_file( $PROCFS . 'cpuinfo' )
		or do {
			$logger->info("Couldn't read ${PROCFS}cpuinfo: $!");
			return $dataref;
	};

	my $cores = grep {/^processor/} @cpu_info;
	# There's always at least 1 core
	$dataref->{sysInfo}->{cpu}->{cores} = $cores || 1;
	#<<<
	($dataref->{sysInfo}->{cpu}->{type} =
		(map { /^(?:model name|Processor)\s+:(.*)$/; $1 || ()} @cpu_info)[0])
		=~ s/^\s+//;
	#>>>
	$dataref->{sysInfo}->{cpu}->{type} =~ s/\s{2,}/ /g;
	$dataref->{sysInfo}->{uptime} =
		( split( /\s+/, slurp_file( $PROCFS . 'uptime' ) ) )[0] or
		$logger->info("Couldn't check ${PROCFS}uptime: $!");

	$dataref->{sysInfo}->{uptime} = $dataref->{sysInfo}->{uptime} +0;

	return $dataref;
}

1;
